package com.example.intent

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    private lateinit var btnIntent: Button
    private lateinit var btnSend: Button
    private lateinit var btnPhone: Button
    private lateinit var edPhone: EditText
    private lateinit var edtInput: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnIntent = findViewById(R.id.secondActivity)
        btnSend = findViewById(R.id.btnSend)
        btnPhone = findViewById(R.id.btnPhone)
        edPhone = findViewById(R.id.edPhone)
        edtInput = findViewById(R.id.edtInput)

        btnIntent.setOnClickListener {
            val showIntent = Intent(this@MainActivity, SecondActivity::class.java)
            startActivity(showIntent)
        }
        btnSend.setOnClickListener {
            sendMessage()
        }
        btnPhone.setOnClickListener {
            val intent =  Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+edPhone.text.toString()))
            startActivity(intent)
        }

    }

    //Explicit With Data
    private fun sendMessage() {
        val message = edtInput.text.toString()
        val intent = Intent(this,SecondActivityWithData::class.java)
        intent.apply {
            putExtra("theMessage", message)
        }
        startActivity(intent)
    }
}