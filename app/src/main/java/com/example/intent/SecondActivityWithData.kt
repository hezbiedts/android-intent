package com.example.intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class SecondActivityWithData : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_with_data)

        val message = intent.getStringExtra("theMessage")

        val tvMsg = findViewById<TextView>(R.id.tv_msg)
        tvMsg.apply {
            text = message
        }
    }
}